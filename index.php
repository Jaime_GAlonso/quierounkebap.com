<?php include 'php/ingredientes.php' ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Quiero un kebap</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!--Importa la hoja de estilos-->
        <link rel="stylesheet" type="text/css" href="css/kebap_styles.css">
        <link rel="stylesheet" type="text/css" href="css/pedidos_styles.css">
        
        <!--Fuentes, importar todas aquí-->
        <link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet' type="text/css">
        
        <!--Fuentes END-->
    </head>
    
    
    
    <body>
        <div id="importNavbar"></div>  <!--Barra de navegacion (importada)-->
        <!-- ===========================================================================================================
        CAROUSEL CON OFERTAS
        =============================================================================================================-->
        <div class = "container" id="carousel-container">
            <div id="Carousel-ofertas" class="carousel slide" data-ride="carousel">
                
                <!--puntitos abajo para cambiar entre ofertas-->
                <ol class="carousel-indicators">
                    <li data-target="#Carousel-ofertas" data-slide-to="0" class="active"></li>
                    <li data-target="#Carousel-ofertas" data-slide-to="1"></li>
                    <li data-target="#Carousel-ofertas" data-slide-to="2"></li>
                </ol>
                
                <!--Ofertas-->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="imagenes/ofetas-carousel/oferta-1.jpg" alt="oferta" style="width:100%;">
                        <div class="carousel-caption">
                            <h1>7,95€</h1>
                            <p class="carousel">Menú y postre</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="imagenes/ofetas-carousel/oferta-2.jpg" alt="3 durum 12,95€" style="width:100%; ">
                        <div class="carousel-caption">
                            <h1>12,95€</h1>
                            <p class="carousel">3 dürum de ternera o pollo</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="imagenes/ofetas-carousel/oferta-3.jpg" alt="oferta" style="width:100%">
                    </div>
                </div>
                
                <!--flechitas laterales-->
                <a class="left carousel-control" href="#Carousel-ofertas" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <!-- para ciegos, que interesante -->
                    <span class="sr-only">Previo</span>
                </a>
                <a class="right carousel-control" href="#Carousel-ofertas" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right">
                    <span class="sr-only">Siguiente</span>
                    </span>
                </a>
            </div>
        </div><!-- Fin carousel de ofertas-->
        
        <!--============================================================================================================================
        FOTOS CON MENUS DESPLEGABLES PARA HACER PEDIDOS
        =============================================================================================================================-->
        
        <div class="container" id="pedido-container">
            <a href="#Panel-pedidos">
                <h1><span class="glyphicon glyphicon-chevron-down"></span>&ensp;Haz tu pedido a tu gusto <span class="glyphicon glyphicon-chevron-down"></span></h1>
            </a>
        
            <div class="container" id="Panel-pedidos">
                <div class="row">
                    <!--DURUM-->
                    <div class="image col-md-6">
                        <img src="imagenes/carta/durum.jpg" alt="durum" style="width:100%">
                        <button type="button" class="animated overlay-image" data-toggle="collapse" data-target="#DurumMenu,#DurumMenu-Mobile"><span>Durüm</span><span class="glyphicon glyphicon-chevron-down"></span></button>
                    </div>
                    <!--DURUM INGREDIENTES pantalla es pequeña-->  
                    <div id="DurumMenu-Mobile" class="collapse collapse-font hidden-sm hidden-md hidden-lg">
                        <h1>Dürum</h1>
                        <div class="row">
                            <form method="post" action="php/order.php">
                                <?php amount(); ?>
                                <h2 class="dropdown">INGREDIENTES</h2>
                                <!--ingredientes-->
                                <?php meat();
                                    tomatoe();
                                    lettuce();
                                    onion();
                                    whiteSauce();
                                    redSauce(); ?>
                            
                                <!-- This input have the name of the food choosen-->
                                <input name="cname" type=radio checked value="Single Durum">
                                
                                <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                            </form>    
                        </div>
                    </div>
                    
                    
                    <!--KEBAB-->
                    <div class="image col-md-6">
                        <img src="imagenes/carta/donner-kebap-kebab.jpg" alt="kebab" style="width:100%">
                        <button type="button" class="animated overlay-image" data-toggle="collapse" data-target="#KebabMenu"><span>kebab</span><span class="glyphicon glyphicon-chevron-down"></span></button>
                    </div>
                </div>
                <!--INGREDIENTES KEBAB-->
                <div id="KebabMenu" class="collapse collapse-font">
                    <h1>Döner Kebab</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            <form method="post" action="php/order.php">
                                <?php amount() ?>
                                
                                <div class="row">
                                    <h2 class="dropdown">INGREDIENTES</h2>
                                </div>
                                <!--ingredientes-->
                                <?php meat();
                                    tomatoe();
                                    lettuce();
                                    onion();
                                    whiteSauce();
                                    redSauce(); ?>
                                <!-- This input have the name of the food choosen-->
                                <input name="cname" type=radio checked value="Single Kebab">
                                
                                <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                            </form>    
                        </div>
                
                        <div class="col-sm-6 hidden-xs">
                            <p class="kebab-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        
                    </div>
                </div>
                
                
                <!--Durum INGREDIENTES pantallas grandes-->
                <div id="DurumMenu" class="collapse collapse-font hidden-xs">
                    <h1>Dürum</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            
                            
                            <form method="post" action="php/order.php">
                                <?php amount(); ?>
                                
                                <div class="row">
                                    <h2 class="dropdown">INGREDIENTES</h2>
                                </div>
                                <!--ingredientes-->
                                <?php meat();
                                    tomatoe();
                                    lettuce();
                                    onion();
                                    whiteSauce();
                                    redSauce(); ?>
                                <!-- This input have the name of the food choosen-->
                                <input name="cname" type=radio checked value="Single Durum">
                                
                                <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                            </form>    
            
                            
                        </div>
                        <div class="col-sm-6 hidden-xs">
                            <p class="kebab-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        
                    </div>
                    </div>
                
                
                
                <!--Otros pedidos menos famosos-->
                <div class="row">
                    <div class="image col-md-6">
                        <img src="imagenes/carta/lahmacun.png" alt="lahmacum" class="img-responsive center-block">
                        <button type="button" class="animated overlay-image" data-toggle="collapse" data-target="#" style="width:14.2vw;margin-left:-7.1vw"><span>Lhamacun</span><span class="glyphicon glyphicon-chevron-down"></span></button>
                    </div>
            
                    <div class="image col-md-6">
                        <img src="imagenes/carta/menu_plato_kebab_kebap.png" alt="plato" class="img-responsive center-block">
                        <button type="button" class="animated overlay-image" data-toggle="collapse" data-target="#PlatoMenu"><span>Plato</span><span class="glyphicon glyphicon-chevron-down"></span></button>
                    </div>
                </div>
                
                <!--INGREDIENTES plato-->
                <div id="PlatoMenu" class="collapse collapse-font" >
                    <h1>Plato Kebab</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            <form method="post" action="php/order.php">
                                <?php amount(); ?>
                                <div class="row">
                                    <h2 class="dropdown">INGREDIENTES</h2>
                                </div>
                                <!--ingredientes-->
                                <?php meat(); ?>
                                <input name="tomatoe" type=radio checked value="NaN">
                                <input name="lettuce" type=radio checked value="NaN">
                                <input name="onion" type=radio checked value="NaN">
                                <?php whiteSauce();
                                    redSauce(); ?>

                                <!-- This input have the name of the food choosen-->
                                <input name="cname" type=radio checked value="Single Plato">

                                <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                            </form>    
                        </div>
                
                        <div class="col-sm-6 hidden-xs">
                            <p class="kebab-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        
                    </div>
                </div>
                
                
                
                <!--BEBIDAS-->
                <div class="row">
                    <div class="image">
                        <img src="imagenes/carta/bebidas.jpg" alt="bebidas" style="width:100%"> 
                    </div>
                </div>
            </div>
        </div>
        
        <!--Footer-->
        <div id="importFooter"></div>
        
        <script>
            $(document).ready(function(){
                    $("#importNavbar").load("navbar.html");
                    $("#importFooter").load("footer.html");
            });

            $(".btn-group > .btn").click(function(){
                $(this).siblings().removeClass("active");
                $(this).addClass("active");
                
                $(this).next().prop("checked", true);
            });
            
        //================================================================================================
        //ADD OR REMOVE 1 FROM THE AMOUNT INPUT BY CLICKING THE BUTTONS
        //================================================================================================
            $(".input-group div.input-group-btn button.left ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val())
                value--;
                if (value<0){
                    value = 0;
                }
                $(this).parent().siblings("input").val(value);
                
            })
        
            $(".input-group div.input-group-btn button.right ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val());
                value++;
                if (value>99){
                    value = 99;
                }
                $(this).parent().siblings("input").val(value);    
            })
            
            $(".input-group.cantidad input").focus(function(){
                $(this).select();
            })
        
        //=================================================================================================
        //CLOSE EVERY DROPDOWN EXCEPT THE LAST ONE OPEN
        //=================================================================================================
            $(".animated").click(function(){
                $("div.collapse").collapse("hide");
            })
        //=================================================================================================
        //AJAX SUBMIT
        //=================================================================================================

        

            $("form").submit(function(e){
                e.preventDefault();
                $.ajax({
                    url:'php/order.php',
                    type:'post',
                    data:$(this).serialize(),
                    beforeSend: function(){
                        $("button.trifasic").toggleClass("trifasic trifasic_2");
                    },
                    success: function(){
                        setTimeout(function(){
                            $("button.trifasic_2").toggleClass("trifasic trifasic_2 trifasic_3");
                        }, 2000);
                        
                        setTimeout(function(){
                            $('button.trifasic_3').toggleClass("trifasic_3");
                        }, 3000);
                        
                    }

                });
            });
        </script>
        
    </body>

</html>
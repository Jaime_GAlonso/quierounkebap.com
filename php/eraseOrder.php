<?php 
$index =(int)test_input($POST["elementToErase"]);

session_start();

//remove the info at $index for the order superglobal
unset($_SESSION["order"][(int)$index]);
$_SESSION["order"]= array_values($_SESSION["order"]);

$_SESSION["static_count"]--;


function test_input($data) 
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
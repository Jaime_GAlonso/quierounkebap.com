
<?php  
function meat(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Carne</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group ">
                <button type="button" class="btn btn-default active">Ternera</button>
                <input type="radio" name="meat" checked value="Ternera">
                <button type="button" class="btn btn-default">Pollo</button>
                <input type="radio" name="meat" value="Pollo">
                <button type="button" class="btn btn-default">Mixto</button>
                <input type="radio" name="meat" value="Mixto">
            </div>
        </div>
    </div>
<?php }


function tomatoe(){ ?>
    
    <div class="row">
        <div class="col-xs-4">
            <p>Tomate</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" name="tomatoe" checked value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="tomatoe" value="No">
            </div>
        </div>
    </div>
<?php } 


function lettuce(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Lechuga</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" name="lettuce" checked value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="lettuce" value="No">
            </div>
        </div>
    </div>
<?php }


function onion(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Cebolla</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" name="onion" checked value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="onion" value="No">
            </div>
        </div>
    </div>
<?php }


function redSauce(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Salsa Roja</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" checked name="redSauce" value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="redSauce" value="No">
            </div>
        </div>
    </div>
<?php } 


function whiteSauce(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Salsa Blanca</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" checked name="whiteSauce" value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="whiteSauce" value="No">
            </div>
        </div>
    </div>
<?php } 


function pWhiteSauce(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Salsa Blanca</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" checked name="pWhiteSauce" value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="pWhiteSauce" value="No">
            </div>
        </div>
    </div>
<?php }

function pRedSauce(){ ?>

    <div class="row">
        <div class="col-xs-4">
            <p>Salsa Roja</p>
        </div>
        <div class="col-xs-8">
            <div class="btn-group">
                <button type="button" class="btn btn-default active">Sí</button>
                <input type="radio" checked name="pRedSauce" value="Si">
                <button type="button" class="btn btn-default">No</button>
                <input type="radio" name="pRedSauce" value="No">
            </div>
        </div>
    </div>
<?php }

function amount(){ ?>
    <div class="row">
        <div class="col-xs-4">
            <p>Cantidad</p>
        </div>
        <div class="col-xs-8">
            <div class="input-group cantidad">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default left"><span class="glyphicon glyphicon-chevron-left"></span></button>
                </div>
                <input name="amount" class="form-control cantidad" dir="rtl" maxlength="2" value="1">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default right"><span class="glyphicon glyphicon-chevron-right"></span></button>
                </div>
            </div>
        </div>
    </div>
<?php } 

function menuAmount(){ ?>
    <div class="row">
        <div class="col-xs-4">
            <p>Menús</p>
        </div>
        <div class="col-xs-8">
            <div class="input-group cantidad">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default left"><span class="glyphicon glyphicon-chevron-left"></span></button>
                </div>
                <input name="amount" class="form-control cantidad" dir="rtl" maxlength="2" value="1">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default right"><span class="glyphicon glyphicon-chevron-right"></span></button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

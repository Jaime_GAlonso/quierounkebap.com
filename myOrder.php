<?php include 'php/ingredientes.php' ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Quiero Un Kebab</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        
        
        <!--Importa la hoja de estilos-->
        <link rel="stylesheet" type="text/css" href="css/kebap_styles.css">
        <link rel="stylesheet" type="text/css" href="css/pedidos_styles.css">
        
        <!--Fuentes, importar todas aquí-->
        <link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet' type="text/css">
        
        <!--Fuentes END-->
    </head>
    <body>
        <div id="importNavbar"></div>
        
        <!--Iterate over the orders and displaying them one by one-->
        <?php 
        session_start();
        $iterations=0;

        //case in which user has not ordered yet
        if(!isset($_SESSION["order"]))
        {
            echo "<h1> Aun no hay nada en tu pedido</h1>";
            echo "<h2 class='text-center'>Puedes añadir cosas desde pedidos o desde menus</h2>";
        }
        else if (count($_SESSION["order"])<1)
        {
            echo "<h1> Aun no hay nada en tu pedido</h1>";
            echo "<h2 class='text-center'>Puedes añadir cosas desde pedidos o desde menus</h2>";
        }
        else
        {
            //case in which user has already ordered
            foreach($_SESSION["order"] as $singleOrder)
            {
                $parsedOrder = explode(" " , $singleOrder);
                    ?>
                    <div class="container orderSummary" data-erase="<?php echo $iterations?>" >
                        <h2>
                            <?php 
                            if($parsedOrder[1] == "Menu")
                            { 
                                echo "Menú " . $parsedOrder[2];
                            }
                            else
                            {
                                echo $parsedOrder[2];
                            }
                            ?>
                        </h2>
                        <hr>
                        <div class="collapse" id="<?php echo "extraInfo_".$iterations ?>">
                                    <div class="container">
                                        <div class="media">
                                            <div class="media-left">

                                                <img src="imagenes/carta/<?php  //Chose the right picture forS the order
                                                           switch($parsedOrder[2]){
                                                               case "Kebab":
                                                                   echo "donner-kebap-kebab.jpg";
                                                                   break;
                                                               case "Durum":
                                                                   echo "durum.jpg";
                                                                   break;
                                                               case "Plato":
                                                                   echo "menu_plato_kebab_kebap.png";
                                                                   break;
                                                               case "Falafel":
                                                                   echo "falafel-kebab-kebap";
                                                                   break;           
                                                           } ?>" alt="pedido" style="height:150px">
                                            </div>
                                            <div class="media-body">
                                                <h3 class="media-heading">
                                                    <?php echo $parsedOrder[2]; ?>
                                                </h3>
                                                <div class="row">
                                                    <?php 
                                                        for($i=0;$i<6;$i++)
                                                        {
                                                            //skip if the ingredient do not exist (plato)
                                                            if($parsedOrder[$i+3] =="NaN"){

                                                            }
                                                            else
                                                            {
                                                                echo "<div class=\"col-xs-2\"> ";
                                                                switch($i)
                                                                {
                                                                    case 0:
                                                                        if($parsedOrder[$i+3] == "Ternera")
                                                                        {
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"ternera\" src=\"imagenes/thumbnails-ingredients/ternera.jpg\">";
                                                                        }
                                                                        else if($parsedOrder[$i+3] == "Pollo")
                                                                        {
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"pollo\" src=\"imagenes/thumbnails-ingredients/pollo.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"mixto\" src=\"imagenes/thumbnails-ingredients/mixto.png\">";
                                                                        }
                                                                        break;
                                                                    case 1:
                                                                        if($parsedOrder[$i+3] == "No"){
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"sinTomate\" src=\"imagenes/thumbnails-ingredients/tomatoe_not.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"tomate\" src=\"imagenes/thumbnails-ingredients/tomatoe.jpg\">";
                                                                        }
                                                                        break;
                                                                    case 2:
                                                                        if($parsedOrder[$i+3] == "No"){
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"nolechuga\" src=\"imagenes/thumbnails-ingredients/lettuce_not.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"lechuga\" src=\"imagenes/thumbnails-ingredients/lettuce.jpg\">";
                                                                        }
                                                                        break;
                                                                    case 3:
                                                                        if($parsedOrder[$i+3] == "No"){
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"nocebolla\" src=\"imagenes/thumbnails-ingredients/onion_not.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"cebolla\" src=\"imagenes/thumbnails-ingredients/onion.jpg\">";
                                                                        }
                                                                        break;
                                                                    case 4:
                                                                        if($parsedOrder[$i+3] == "No"){
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"nosalsablanca\" src=\"imagenes/thumbnails-ingredients/salsablanca_not.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"salsablanca\" src=\"imagenes/thumbnails-ingredients/salsablanca.png\">";
                                                                        }
                                                                        break;
                                                                    case 5:
                                                                        if($parsedOrder[$i+3] == "No"){
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"nosalsaroja\" src=\"imagenes/thumbnails-ingredients/salsaroja_not.jpg\">";
                                                                        }
                                                                        else{
                                                                            echo "<img class=\"ingredient-thumb\" alt=\"salsaroja\" src=\"imagenes/thumbnails-ingredients/salsaroja.png\">";
                                                                        }
                                                                        break;
                                                                }
                                                                echo "</div>";
                                                            }

                                                        } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                                //if the order is a menu
                                if($parsedOrder[1]=="Menu")
                                { ?>
                                <div class="container">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="imagenes/carta/patatas-fritas-crujientes.jpg" alt="patatas" style="height:200px;">
                                        </div>
                                        <div class="media-body">
                                            <h3 class="media-heading"> Patatas </h3>
                                            <div class="row">
                                                <?php 
                                                    for($i=0;$i<=1;$i++)
                                                    {
                                                        echo "<div class=\"col-xs-2\">";
                                                        switch ($i)
                                                        {
                                                            case 0:
                                                                if($parsedOrder[$i+8] == "No"){
                                                                    echo "<img class=\"ingredient-thumb\" alt=\"noSalsablanca\" src=\"imagenes/thumbnails-ingredients/salsablanca_not.jpg\">";
                                                                }
                                                                else{
                                                                    echo "<img class=\"ingredient-thumb\" alt=\"salsablanca\" src=\"imagenes/thumbnails-ingredients/salsablanca.png\">";
                                                                }
                                                                break;
                                                            case 1:
                                                                if($parsedOrder[$i+8] == "No"){
                                                                    echo "<img class=\"ingredient-thumb\" alt=\"nosalsaroja\" src=\"imagenes/thumbnails-ingredients/salsaroja_not.jpg\">";
                                                                }
                                                                else{
                                                                    echo "<img class=\"ingredient-thumb\" alt=\"salsaroja\" src=\"imagenes/thumbnails-ingredients/salsaroja.png\">";
                                                                }
                                                                break;
                                                        }
                                                        echo "</div>";
                                                    } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                          <?php  } //Close the if statment about chips existence ?>  

                        </div>


                        <!--Buttons for details and remove-->
                        <div class="row">
                            <div class="col-xs-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                        <p>Cantidad</p>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="input-group cantidad">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default left"><span class="glyphicon glyphicon-chevron-left"></span></button>
                                            </div>
                                            <input name="amount" class="form-control cantidad" dir="rtl" maxlength="2" value="<?php echo $parsedOrder[0];?>" >
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default right"><span class="glyphicon glyphicon-chevron-right"></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <p>Precio</p>
                            </div>
                            <div class="col-xs-3">
                                <!--insert price with php AJAX, must change when user modifies parameters of the order-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <button type="button" class="animated" data-toggle="collapse" data-target="<?php echo "#extraInfo_".$iterations;?>"> <span>Detalles</span><span class="glyphicon glyphicon-chevron-down"></span></button>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" class="animated" data-erase="true"><span>Quitar</span><span class="glyphicon glyphicon-remove"></span></button>
                            </div>
                        </div>
                    </div>

            <?php
                $iterations++;
                }
        }//end else
        ?>
        
        
        
        
        
        
        <script>
            $(document).ready(function(){
                    $("#importNavbar").load("navbar.html");
                    $("#importFooter").load("footer.html");
            });
            
            //================================================================================================
            //ADD OR REMOVE 1 FROM THE AMOUNT INPUT BY CLICKING THE BUTTONS
            //================================================================================================
            $(".input-group div.input-group-btn button.left ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val());
                value--;
                if (value<0){
                    value = 0;
                }
                $(this).parent().siblings("input").val(value);

            });

            $(".input-group div.input-group-btn button.right ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val());
                value++;
                if (value>99){
                    value = 99;
                }
                $(this).parent().siblings("input").val(value);    
            });

            $(".input-group.cantidad input").focus(function(){
                $(this).select();
            });
            //================================================================================================
            //DELETE ANYTHING FROM THE ORDER
            //================================================================================================
            
             $("button[data-erase=true]").on("click",function(e){
                e.preventDefault();
                var numberToErase = $(this).closest(".orderSummary").attr("data-erase"); 
                console.log(numberToErase); 
                $.ajax({
                    url:'php/eraseOrder.php',
                    type:'post',
                    data:'elementToErase='+numberToErase,
                    beforeSend: function(){
                        console.log("gotcha");
                    },
                    success: function(){
                        $(".orderSummary[data-erase="+numberToErase+"]").css({"display":"none"});
                    }

                });
            });
            
        </script>

    
    </body>
</html>
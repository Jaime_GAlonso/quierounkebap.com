<!DOCTYPE html>
<html>
    
    
<head>
    <meta charset="utf-8">
        <title>Quiero un kebab</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!--Importa la hoja de estilos-->
        <link rel="stylesheet" type="text/css" href="css/kebap_styles.css">
         <link rel="stylesheet" type="text/css" href="css/pedidos_styles.css">
    
        <!--Fuentes, importar todas aquí-->
        <link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet' type="text/css">
        
        <!--Fuentes END-->
        
        <!-- importa la cabecera (y otras cosas) con jQuery AJAX-->
        <script>
            $(document).ready(function(){
                    $("#importNavbar").load("navbar.html");
                    $("#importFooter").load("footer.html");
            });
        </script>
   <style>
    .intentotitulo3{
                font-size: 1vw;
                color: black;
                text-align: center;
                font-family: 'Aclonica';
       }
        </style>

</head>  
    
    
<body>
<div class="container-fluid" id="top-container"></div> <!--Zona superior a la barra de navegación-->
<div id="importNavbar"></div>
<h1>SOBRE NOSOTROS</h1>
    
    
<div class="container" id="DONDE">
    <div class="image col-md-6">
        
        <h2 class=intentotitulo2>¿Dónde estamos?</h2>
        
        <p class="kebab-description">¡Ven a visitarnos!<br>
        Somos una pequeña empresa con un local ubicado en Gijón, Asturias.<br>
        </p>
        
    </div>
    
    
    <div class="image col-md-6">
        
        <iframe class="img-responsive center-block" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6189.451497189997!2d-5.627890532682353!3d43.52222831718221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc206faed6b67202e!2sUniversidad+de+Oviedo%3A+Campus+de+Gij%C3%B3n!5e0!3m2!1ses!2ses!4v1512591599806" style="border:4px solid orangered; height:35vw!important; width:40vw" allowfullscreen ></iframe>
    
    </div>
    
</div>
    
<div class="container" id="QUIEN">
<h2 class=intentotitulo2>¿Quiénes somos?</h2>
     
<p class="kebab-description">Puedes encontrar toda nuestra información a traves de estos QRs.</p>      
    <div class="image col-md-6" id="Descripion-miembro1">
        
        <img class="img-responsive center-block" src="imagenes/quienessomos/Teresa.jpg" alt="Teresa" width="300">
        
        <p class="intentotitulo3"> Nombre y apellidos:</p>
        <p class="kebab-description"> Teresa Isabel <br>Rey Alonso</p>
        <p class="intentotitulo3"> Cargo:</p>
        <p class="intentotitulo3"> Estudiante de Telecomunicaciones</p>
        <p class="intentotitulo3"> Empresa:</p>
        <p class="intentotitulo3"> Quiero Un Kebab</p>
        <p class="intentotitulo3"> Direccion:</p>
        <p class="intentotitulo3"> Oviedo</p>
        <p class="intentotitulo3"> Email:</p>
        <p class="intentotitulo3"> uo250324</p>    
     
        <img class="img-responsive center-block" src="http://www.codigos-qr.com/qr/php/qr_img.php?d=BEGIN%3AVCARD%0AN%3ATeresa+Rey+Alonso%0ATITLE%3AEstudiante+de+Telecomunicaciones%0AORG%3A%0AADDR%3AOviedo%0ATEL%3A%0AEMAIL%3Auo250324%40uniovi.es%0AURL%3A%0AEND%3AVCARD&s=8&e=" style="width:15vw" alt="Generador de Códigos QR Codes"/>

    </div>
    
    
     <div class="image col-md-6" id="Descripion-miembro2">
         
        <img class="img-responsive center-block" src="imagenes/quienessomos/Jaime.png" alt="Jaime" width="300">
         
        <p class="intentotitulo3"> Nombre y apellidos:</p>
        <p class="kebab-description"> Jaime <br>Gonzalez Alonso</p>
        <p class="intentotitulo3"> Cargo:</p>
        <p class="intentotitulo3"> Estudiante de Telecomunicaciones</p>
        <p class="intentotitulo3"> Empresa:</p>
        <p class="intentotitulo3"> Quiero Un Kebab</p>
        <p class="intentotitulo3"> Direccion:</p>
        <p class="intentotitulo3"> Aviles</p>
        <p class="intentotitulo3"> Email:</p>
        <p class="intentotitulo3"> uo231344</p>
         
         
        <img class="img-responsive center-block" src="http://www.codigos-qr.com/qr/php/qr_img.php?d=BEGIN%3AVCARD%0AN%3AJaime+Gonzalez+Alonso%0ATITLE%3AEstudiante+de+Telecomunicaciones%0AORG%3A%0AADDR%3AAviles%0ATEL%3A%0AEMAIL%3Auo231344%40uniovi.es%0AURL%3A%0AEND%3AVCARD&s=8&e=" style="width:15vw" alt="Generador de Códigos QR Codes"/>
         
    </div> 
</div>
    
<div id="importFooter"></div>
</body>
    
    
</html>

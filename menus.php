<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Quiero Un Kebab</title>
        
         <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" type="text/css" href="css/kebap_styles.css">
        <link rel="stylesheet" type="text/css" href="css/pedidos_styles.css">
        <link rel="stylesheet" type="text/css" href="css/menus_styles.css">
        
        <!--serverSide include-->
        <?php include 'php/ingredientes.php' ?>
        
        <!--Fonts-->
        <link href='https://fonts.googleapis.com/css?family=Aclonica' rel='stylesheet' type="text/css">
        
    </head>
    
    <body>
        <div id="importNavbar"></div>
        
        <div class="container">
            <h1>Menús Completos</h1>
            <h2 class="blackText">La forma más rápida y sencilla de hacer tu pedido</h2>
        </div>
        
        <!--Menu tags-->
        <div class="container" id="MenuSetContainer">
            <div class="container singleMenu">
                <div class="row menu">
                    <div class="col-xs-4">
                        <img style="width:100%" alt="menu kebab" src="imagenes/ofetas-carousel/oferta-1.jpg">
                    </div>
                    <div class="col-xs-8">
                        <p>Kebab con patatas y refresco 6.00€</p>
                    </div>
                </div>
            </div>
            
            <div class="collapse collapse-font" id="menu1">
                <form action="php/order.php" method="post">
                    <div class="col-md-6">
                        <?php menuAmount() ?>
                        
                        <h1>Kebab</h1>
                        <h2 class="blackText">Ingredientes</h2>

                        <?php meat();
                            tomatoe();
                            lettuce();
                            onion();
                            whiteSauce(); 
                            redSauce();?>    
                    </div>
                    <div class="col-md-6">
                        <h1>Patatas</h1>
                        <?php pWhiteSauce();
                            pRedSauce(); ?>

                        <input name="cname" type="radio" checked value="Menu Kebab">
                    </div>
                    <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                </form>
            </div>
            <!--Button that open and close the dropdown-->
            <button type="button" class="btn btn-default btn-menu" data-target="#menu1" data-toggle="collapse"><span class="glyphicon glyphicon-chevron-down"></span></button>
            

            <div class="container">
                <div class="row menu">
                    <div class="col-xs-4">
                        <img style="width:100%" alt="menu durum" src="imagenes/ofetas-carousel/oferta-1.jpg">
                    </div>
                    <div class="col-xs-8">
                        <p>Dürum con patatas y refresco 7.00€</p>
                    </div>
                </div>
            </div>
            
            <div class="collapse collapse-font" id="menu2">
                <form action="php/order.php" method=post>
                    <div class="col-md-6">
                        <?php menuAmount() ?>
                        
                        <h1>Dürum</h1>
                        <h2 class="blackText">Ingredientes</h2>

                        <?php meat();
                            tomatoe();
                            lettuce();
                            onion();
                            whiteSauce(); 
                            redSauce();?>      
                    </div>
                    <div class="col-md-6">
                        <h1>Patatas</h1>
                        <?php  pWhiteSauce(); 
                            pRedSauce();?>  


                    </div>
                    <input type="radio" checked name="cname" value="Menu Durum"> 
                    <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                 </form>
            </div>
            <!--Button that open and close the dropdown-->
            <button type="button" class="btn btn-default btn-menu" data-target="#menu2" data-toggle="collapse"><span class="glyphicon glyphicon-chevron-down"></span></button>
            

            <div class="container">
                <div class="row menu">
                    <div class="col-xs-4">
                        <img style="width:100%" alt="menu plato" src="imagenes/ofetas-carousel/oferta-1.jpg">
                    </div>
                    <div class="col-xs-8">
                        <p>Plato con patatas y refresco 7.50€</p>
                    </div>
                </div>
            </div>
            
            <div class="collapse collapse-font" id="menu4">
                <form action="php/order.php" method="post">
                    <div class="col-md-6">
                        <?php menuAmount() ?>

                        <h1>Plato</h1>
                        <h2 class="blackText">Ingredientes</h2>

                            <?php meat();
                                whiteSauce(); 
                                redSauce();?>       
                    </div>
                    <div class="col-md-6">
                        <h1>Patatas</h1>
                        <?php  pWhiteSauce(); 
                            pRedSauce();?>  
                    </div>
                    <input type="radio" checked name="cname" value="Menu Plato"> 
                    <button type="submit" class="trifasic"><span>Añadir</span><span>Hecho</span></button>
                </form>
            </div>
        
            <!--Button that open and close the dropdown-->
            <button type="button" class="btn btn-default btn-menu" data-target="#menu4" data-toggle="collapse"><span class="glyphicon glyphicon-chevron-down"></span></button>
        </div><!--Big container end-->

        <div id="importFooter"></div>
        
        
         <script>
         $(document).ready(function(){
            $("#importNavbar").load("navbar.html");
            $("#importFooter").load("footer.html");
        });

        
        $(".btn-group > .btn").click(function(){
            $(this).siblings().removeClass("active");
            $(this).addClass("active");

            $(this).children().prop("checked", true);
        });
        
        $(".btn-menu").click(function(){
            if ($(this).children().hasClass("glyphicon-chevron-down"))
            {
                $(this).children().removeClass("glyphicon-chevron-down");
                $(this).children().addClass("glyphicon-chevron-up");  
            }
            else
            {
                $(this).children().removeClass("glyphicon-chevron-up");
                $(this).children().addClass("glyphicon-chevron-down");  
            }
            
        })
        
        //================================================================================================
        //ADD OR REMOVE 1 FROM THE AMOUNT INPUT BY CLICKING THE BUTTONS
        //================================================================================================
            $(".input-group div.input-group-btn button.left ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val())
                value--;
                if (value<0){
                    value = 0;
                }
                $(this).parent().siblings("input").val(value);
                
            })
        
            $(".input-group div.input-group-btn button.right ").click(function(){
                var value = parseInt($(this).parent().siblings("input").val());
                value++;
                if (value>99){
                    value = 99;
                }
                $(this).parent().siblings("input").val(value);    
            })
            
            $(".input-group.cantidad input").focus(function(){
                $(this).select();
            })
        
        
        //=================================================================================================
        //AJAX SUBMIT
        //=================================================================================================

        

            $("form").submit(function(e){
                e.preventDefault();
                $.ajax({
                    url:'php/order.php',
                    type:'post',
                    data:$(this).serialize(),
                    beforeSend: function(){
                        $("button.trifasic").toggleClass("trifasic trifasic_2");
                    },
                    success: function(){
                        setTimeout(function(){
                            $("button.trifasic_2").toggleClass("trifasic trifasic_2 trifasic_3");
                        }, 2000);
                        
                        setTimeout(function(){
                            $('button.trifasic_3').toggleClass("trifasic_3");
                        }, 3000);
                        
                    }

                });
            });
        
    </script>
    </body> 
    
    
</html>